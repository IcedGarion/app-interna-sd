## USER SERVICE

### TODO
- gestire attributo ruolo admin
- permettere a redmineIntegrator di patchare tutti i campi tranne gli extra; permettere agli utenti di patchare solo i campi extra
  (per ora tutti possono modificare tutto, vedi UserCreateOrUpdateRequestBean)

- userId poi da cancellare, basta UUID

- (UserService.deleteUser ha bisogno di @Transactional altrimenti errore: 
  "No EntityManager with actual transaction available for current thread - cannot reliably process 'remove' call")
