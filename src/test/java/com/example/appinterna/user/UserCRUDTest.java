package com.example.appinterna.user;

import com.example.appinterna.user.controller.bean.UserCreateOrUpdateRequestBean;
import com.example.appinterna.user.controller.bean.UserResponseBean;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserCRUDTest {

	@Autowired
	private MockMvc mockMvc;

	private void assertUserBeansEquals(UserCreateOrUpdateRequestBean expected, UserResponseBean actual, boolean checkDescrizioneTelefono) {
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getEmail(), actual.getEmail());
		assertEquals(expected.getInizioContratto(), actual.getInizioContratto());
		assertEquals(expected.getSkillPrimario(), actual.getSkillPrimario());
		assertEquals(expected.getCdc(), actual.getCdc());
		assertEquals(expected.getSedeEffettiva(), actual.getSedeEffettiva());
		assertEquals(expected.getLivello(), actual.getLivello());
		assertEquals(expected.getRegistratoIl(), actual.getRegistratoIl());
		assertEquals(expected.getInizioRapporto(), actual.getInizioRapporto());
		assertEquals(expected.getTipo(), actual.getTipo());
		assertEquals(expected.getSkillSecondari(), actual.getSkillSecondari());
		assertEquals(expected.getSedeAmministrativa(), actual.getSedeAmministrativa());
		assertEquals(expected.getCapability(), actual.getCapability());
		assertEquals(expected.getBusinessUnit(), actual.getBusinessUnit());
		assertEquals(expected.getRuolo(), actual.getRuolo());
		if(checkDescrizioneTelefono) {
			assertEquals(expected.getDescrizione(), actual.getDescrizione());
			assertEquals(expected.getTelefono(), actual.getTelefono());
		}
	}

	private void assertUserBeansEquals(UserCreateOrUpdateRequestBean expected, UserResponseBean actual) {
		assertUserBeansEquals(expected, actual, true);
	}

	@Test
	@Transactional
	public void testGet() throws Exception {
		// test vuoto /users
		MvcResult result = mockMvc.perform(get("/api/users"))
				.andExpect(status().isOk())
				.andReturn();

		ObjectMapper objectMapper = new ObjectMapper();
		List<UserResponseBean> userResponseBeans = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() { });
		assertTrue(userResponseBeans.isEmpty());

		// test 1 record /users/userId
		UserCreateOrUpdateRequestBean userRequestBean = new UserCreateOrUpdateRequestBean("testName", "testEmail", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("17/08/2021"), "testSkill", "testCdc", "testSede", "testLivello", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("18/08/2021"), new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("19/08/2021"), "testTipo", "testSkillSecondari", "testSedeAmministrativa", "testCpability", "testBusinessUnit", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("20/08/2021"), "user", "testDescrizione", "0000");
		String json = objectMapper.writeValueAsString(userRequestBean);
		mockMvc.perform(put("/api/users/200")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json));

		result = mockMvc.perform(get("/api/users"))
				.andExpect(status().isOk())
				.andReturn();
		userResponseBeans = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() { });
		assertEquals(1, userResponseBeans.size());
		UserResponseBean userResponseBean = userResponseBeans.get(0);
		assertUserBeansEquals(userRequestBean, userResponseBean);

		// test 1 record /users
		result = mockMvc.perform(get("/api/users/200"))
				.andExpect(status().isOk())
				.andReturn();

		userResponseBean = objectMapper.readValue(result.getResponse().getContentAsString(), UserResponseBean.class);
		assertUserBeansEquals(userRequestBean, userResponseBean);


		// TODO: aggiungere le stesse operazioni di su, pero' anche con la POST + 2 verifiche GET, invece che solo la PUT
	}

	@Test
	@Transactional
	public void testPost() throws Exception {
		UserCreateOrUpdateRequestBean userRequestBean = new UserCreateOrUpdateRequestBean("testName", "testEmail", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("17/08/2021"), "testSkill", "testCdc", "testSede", "testLivello", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("18/08/2021"), new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("19/08/2021"), "testTipo", "testSkillSecondari", "testSedeAmministrativa", "testCpability", "testBusinessUnit", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("20/08/2021"), "user", "testDescrizione", "0000");
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(userRequestBean);

		MvcResult result = mockMvc.perform(post("/api/users")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
				.andExpect(status().isCreated())
				.andReturn();

		UserResponseBean userResponseBean = objectMapper.readValue(result.getResponse().getContentAsString(), UserResponseBean.class);
		assertUserBeansEquals(userRequestBean, userResponseBean);
	}

	@Test
	@Transactional
	public void testDelete() throws Exception {
		UserCreateOrUpdateRequestBean userRequestBean = new UserCreateOrUpdateRequestBean("testName", "testEmail", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("17/08/2021"), "testSkill", "testCdc", "testSede", "testLivello", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("18/08/2021"), new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("19/08/2021"), "testTipo", "testSkillSecondari", "testSedeAmministrativa", "testCpability", "testBusinessUnit", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("20/08/2021"), "user", "testDescrizione", "0000");
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(userRequestBean);

		mockMvc.perform(put("/api/users/10")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
				.andExpect(status().isCreated());

		mockMvc.perform(delete("/api/users/10"))
				.andExpect(status().isOk());

		mockMvc.perform(get("/api/users/10"))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void testPut() throws Exception {
		UserCreateOrUpdateRequestBean userRequestBean = new UserCreateOrUpdateRequestBean("testName", "testEmail", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("17/08/2021"), "testSkill", "testCdc", "testSede", "testLivello", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("18/08/2021"), new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("19/08/2021"), "testTipo", "testSkillSecondari", "testSedeAmministrativa", "testCpability", "testBusinessUnit", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("20/08/2021"), "user", "testDescrizione", "0000");
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(userRequestBean);

		// inserimento iniziale
		mockMvc.perform(put("/api/users/20")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
				.andExpect(status().isCreated());

		userRequestBean = new UserCreateOrUpdateRequestBean("testName2", "testEmail2", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("17/08/2020"), "testSkill2", "testCdc2", "testSede2", "testLivello2", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("18/08/2020"), new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("19/08/2020"), "testTipo2", "testSkillSecondari2", "testSedeAmministrativa2", "testCpability2", "testBusinessUnit2", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("20/08/2020"), "admin", null, null);
		json = objectMapper.writeValueAsString(userRequestBean);

		// test replace
		MvcResult result = mockMvc.perform(put("/api/users/20")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
				.andExpect(status().isOk())
				.andReturn();

		UserResponseBean userResponseBean = objectMapper.readValue(result.getResponse().getContentAsString(), UserResponseBean.class);
		assertUserBeansEquals(userRequestBean, userResponseBean);

		// test create
		userRequestBean = new UserCreateOrUpdateRequestBean("testName3", "testEmail3", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("17/08/2022"), "testSkill3", "testCdc3", "testSede3", "testLivello3", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("18/08/2022"), new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("19/08/2022"), "testTipo3", "testSkillSecondari3", "testSedeAmministrativa3", "testCpability3", "testBusinessUnit3", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("20/08/2022"), "user", null, null);
		json = objectMapper.writeValueAsString(userRequestBean);

		result = mockMvc.perform(put("/api/users/30")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
				.andExpect(status().isCreated())
				.andReturn();

		userResponseBean = objectMapper.readValue(result.getResponse().getContentAsString(), UserResponseBean.class);
		assertUserBeansEquals(userRequestBean, userResponseBean, false);
		assertNull(userResponseBean.getDescrizione());
		assertNull(userResponseBean.getTelefono());
	}

	@Test
	@Transactional
	public void testPatch() throws Exception{
		UserCreateOrUpdateRequestBean userRequestBean = new UserCreateOrUpdateRequestBean("testName", "testEmail", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("17/08/2021"), "testSkill", "testCdc", "testSede", "testLivello", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("18/08/2021"), new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("19/08/2021"), "testTipo", "testSkillSecondari", "testSedeAmministrativa", "testCpability", "testBusinessUnit", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("20/08/2021"), "user", "testDescrizione", "0000");
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(userRequestBean);

		mockMvc.perform(put("/api/users/40")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
				.andExpect(status().isCreated());

		userRequestBean = new UserCreateOrUpdateRequestBean("testName2", "testEmail2", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("17/08/2020"), "testSkill2", "testCdc2", "testSede2", "testLivello2", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("18/08/2020"), new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("19/08/2020"), "testTipo2", "testSkillSecondari2", "testSedeAmministrativa2", "testCpability2", "testBusinessUnit2", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("20/08/2020"), "admin", null, null);
		json = objectMapper.writeValueAsString(userRequestBean);

		MvcResult result = mockMvc.perform(patch("/api/users/40")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
				.andExpect(status().isOk())
				.andReturn();

		UserResponseBean userResponseBean = objectMapper.readValue(result.getResponse().getContentAsString(), UserResponseBean.class);

		// test 404
		mockMvc.perform(patch("/api/users/404")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
				.andExpect(status().isNotFound());
	}
}
