package com.example.appinterna.user;

import com.example.appinterna.user.controller.bean.UserCreateOrUpdateRequestBean;
import com.example.appinterna.user.messaging.KafkaConfig;
import com.example.appinterna.user.messaging.KafkaConsumerUser;
import com.example.appinterna.user.messaging.KafkaProducerUser;
import com.example.appinterna.user.messaging.bean.UserEventEnum;
import com.example.appinterna.user.service.bean.UserBean;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class UserMessagingTest {

	@Autowired
	private KafkaProducerUser producer;

	@Autowired
	private KafkaConsumerUser consumer;

	@Test
	void testSendReceive() throws InterruptedException, ParseException {
		UserBean payload = new UserBean(1L, "testName", "testEmail", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("17/08/2021"), "testSkill", "testCdc", "testSede", "testLivello", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("18/08/2021"), new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("19/08/2021"), "testTipo", "testSkillSecondari", "testSedeAmministrativa", "testCpability", "testBusinessUnit", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("20/08/2021"), "user", "testDescrizione", "0000");

		// send
		producer.send(UserEventEnum.USER_CREATED, payload);

		// receive
		consumer.getLatch().await(10000, TimeUnit.MILLISECONDS);
		UserBean response = consumer.getPayload();

		assertEquals(0, consumer.getLatch().getCount());
		assertEquals(KafkaConfig.USER_KAFKA_TOPIC, consumer.getMessage().topic());
		assertEquals(UserEventEnum.USER_CREATED, consumer.getEvent());
		assertEquals(payload, response);
	}


	// TODO
	@Test
	void testUserCreateWithEvent() throws ParseException {
		UserCreateOrUpdateRequestBean userRequestBean = new UserCreateOrUpdateRequestBean("testName", "testEmail", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("17/08/2021"), "testSkill", "testCdc", "testSede", "testLivello", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("18/08/2021"), new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("19/08/2021"), "testTipo", "testSkillSecondari", "testSedeAmministrativa", "testCpability", "testBusinessUnit", new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).parse("20/08/2021"), "user", "testDescrizione", "0000");
	}

	@Test
	void testUserUpdateWithEvent() {

	}

	@Test
	void testUserDeleteWithEvent() {

	}
}
