package com.example.appinterna.user.persistence.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Entity
public class UserEntity {
    @Id @GeneratedValue
    @Column(name = "uuid")
    private UUID uuid;

    // per la PUT; si puo' togliere se si vuole mantenere solo uuid
    @Column(unique = true, name = "user_id")
    @GenericGenerator(name = "UseExistingIdOtherwiseGenerateUsingIdentity", strategy = "com.example.appinterna.user.persistence.UseExistingIdOtherwiseGenerateUsingIdentity")
    @GeneratedValue(generator = "UseExistingIdOtherwiseGenerateUsingIdentity")
    private Long userId;

    private String name;
    private String email;
    private Date inizioContratto;
    private String skillPrimario;
    private String cdc;
    private String sedeEffettiva;
    private String livello;
    private Date registratoIl;
    private Date inizioRapporto;
    private String tipo;
    private String skillSecondari;
    private String sedeAmministrativa;
    private String capability;
    private String businessUnit;
    private Date ultimaConnessione;

    // attributi extra-redmine
    private String ruolo;
    private String descrizione;
    private String telefono;

    public UserEntity() {}

    public UserEntity(Long userId, String name, String email, Date inizioContratto, String skillPrimario, String cdc, String sedeEffettiva, String livello, Date registratoIl, Date inizioRapporto, String tipo, String skillSecondari, String sedeAmministrativa, String capability, String businessUnit, Date ultimaConnessione, String ruolo, String descrizione, String telefono) {
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.inizioContratto = inizioContratto;
        this.skillPrimario = skillPrimario;
        this.cdc = cdc;
        this.sedeEffettiva = sedeEffettiva;
        this.livello = livello;
        this.registratoIl = registratoIl;
        this.inizioRapporto = inizioRapporto;
        this.tipo = tipo;
        this.skillSecondari = skillSecondari;
        this.sedeAmministrativa = sedeAmministrativa;
        this.capability = capability;
        this.businessUnit = businessUnit;
        this.ultimaConnessione = ultimaConnessione;
        this.ruolo = ruolo;
        this.descrizione = descrizione;
        this.telefono = telefono;
    }

    public UserEntity(String name, String email, Date inizioContratto, String skillPrimario, String cdc, String sedeEffettiva, String livello, Date registratoIl, Date inizioRapporto, String tipo, String skillSecondari, String sedeAmministrativa, String capability, String businessUnit, Date ultimaConnessione, String ruolo, String descrizione, String telefono) {
        this.name = name;
        this.email = email;
        this.inizioContratto = inizioContratto;
        this.skillPrimario = skillPrimario;
        this.cdc = cdc;
        this.sedeEffettiva = sedeEffettiva;
        this.livello = livello;
        this.registratoIl = registratoIl;
        this.inizioRapporto = inizioRapporto;
        this.tipo = tipo;
        this.skillSecondari = skillSecondari;
        this.sedeAmministrativa = sedeAmministrativa;
        this.capability = capability;
        this.businessUnit = businessUnit;
        this.ultimaConnessione = ultimaConnessione;
        this.ruolo = ruolo;
        this.descrizione = descrizione;
        this.telefono = telefono;
    }

    public UUID getUuid() { return uuid; }

    public String getName() {
        return name;
    }

    public Long getUserId() {
        return userId;
    }

    // getter
    public String getEmail() {
        return email;
    }

    public Date getInizioContratto() {
        return inizioContratto;
    }

    public String getSkillPrimario() {
        return skillPrimario;
    }

    public String getCdc() {
        return cdc;
    }

    public String getSedeEffettiva() {
        return sedeEffettiva;
    }

    public String getLivello() {
        return livello;
    }

    public Date getRegistratoIl() {
        return registratoIl;
    }

    public Date getInizioRapporto() {
        return inizioRapporto;
    }

    public String getTipo() {
        return tipo;
    }

    public String getSkillSecondari() {
        return skillSecondari;
    }

    public String getSedeAmministrativa() {
        return sedeAmministrativa;
    }

    public String getCapability() {
        return capability;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public Date getUltimaConnessione() {
        return ultimaConnessione;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public String getRuolo() {
        return ruolo;
    }

    public String getTelefono() {
        return telefono;
    }

    // setter
    // poi rimarranno soltanto i "setIfNotNull", una volta eliminata la PUT
    public UserEntity setName(String name) {
        this.name = name;
        return this;
    }

    public UserEntity setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserEntity setInizioContratto(Date inizioContratto) {
        this.inizioContratto = inizioContratto;
        return this;
    }

    public UserEntity setSkillPrimario(String skillPrimario) {
        this.skillPrimario = skillPrimario;
        return this;
    }

    public UserEntity setCdc(String cdc) {
        this.cdc = cdc;
        return this;
    }

    public UserEntity setSedeEffettiva(String sedeEffettiva) {
        this.sedeEffettiva = sedeEffettiva;
        return this;
    }

    public UserEntity setLivello(String livello) {
        this.livello = livello;
        return this;
    }

    public UserEntity setRegistratoIl(Date registratoIl) {
        this.registratoIl = registratoIl;
        return this;
    }

    public UserEntity setInizioRapporto(Date inizioRapporto) {
        this.inizioRapporto = inizioRapporto;
        return this;
    }

    public UserEntity setTipo(String tipo) {
        this.tipo = tipo;
        return this;
    }

    public UserEntity setSkillSecondari(String skillSecondari) {
        this.skillSecondari = skillSecondari;
        return this;
    }

    public UserEntity setSedeAmministrativa(String sedeAmministrativa) {
        this.sedeAmministrativa = sedeAmministrativa;
        return this;
    }

    public UserEntity setCapability(String capability) {
        this.capability = capability;
        return this;
    }

    public UserEntity setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
        return this;
    }

    public UserEntity setUltimaConnessione(Date ultimaConnessione) {
        this.ultimaConnessione = ultimaConnessione;
        return this;
    }

    public UserEntity setRuolo(String ruolo) {
        this.ruolo = ruolo;
        return this;
    }

    public UserEntity setDescrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public UserEntity setTelefono(String telefono) {
        this.telefono = telefono;
        return this;
    }

    public UserEntity setUserId(Long userId) {
        this.userId = userId;
        return this;
    }


    // setter per le patch (setta solo se parametro != null)
    public UserEntity setNameIfNotNull(String name) {
        this.name = name != null ? name : this.name;
        return this;
    }

    public UserEntity setEmailIfNotNull(String email) {
        this.email = email != null ? email : this.email;
        return this;
    }

    public UserEntity setInizioContrattoIfNotNull(Date inizioContratto) {
        this.inizioContratto = inizioContratto != null ? inizioContratto : this.inizioContratto;
        return this;
    }

    public UserEntity setSkillPrimarioIfNotNull(String skillPrimario) {
        this.skillPrimario = skillPrimario != null ? skillPrimario : this.skillPrimario;
        return this;
    }

    public UserEntity setCdcIfNotNull(String cdc) {
        this.cdc = cdc != null ? cdc : this.cdc;
        return this;
    }

    public UserEntity setSedeEffettivaIfNotNull(String sedeEffettiva) {
        this.sedeEffettiva = sedeEffettiva != null ? sedeEffettiva : this.sedeEffettiva;
        return this;
    }

    public UserEntity setLivelloIfNotNull(String livello) {
        this.livello = livello != null ? livello : this.livello;
        return this;
    }

    public UserEntity setRegistratoIlIfNotNull(Date registratoIl) {
        this.registratoIl = registratoIl != null ? registratoIl : this.registratoIl;
        return this;
    }

    public UserEntity setInizioRapportoIfNotNull(Date inizioRapporto) {
        this.inizioRapporto = inizioRapporto != null ? inizioRapporto : this.inizioRapporto;
        return this;
    }

    public UserEntity setTipoIfNotNull(String tipo) {
        this.tipo = tipo != null ? tipo : this.tipo;
        return this;
    }

    public UserEntity setSkillSecondariIfNotNull(String skillSecondari) {
        this.skillSecondari = skillSecondari != null ? skillSecondari : this.skillSecondari;
        return this;
    }

    public UserEntity setSedeAmministrativaIfNotNull(String sedeAmministrativa) {
        this.sedeAmministrativa = sedeAmministrativa != null ? sedeAmministrativa : this.sedeAmministrativa;
        return this;
    }

    public UserEntity setCapabilityIfNotNull(String capability) {
        this.capability = capability != null ? capability : this.capability;
        return this;
    }

    public UserEntity setBusinessUnitIfNotNull(String businessUnit) {
        this.businessUnit = businessUnit != null ? businessUnit : this.businessUnit;
        return this;
    }

    public UserEntity setUltimaConnessioneIfNotNull(Date ultimaConnessione) {
        this.ultimaConnessione = ultimaConnessione != null ? ultimaConnessione : this.ultimaConnessione;
        this.ultimaConnessione = ultimaConnessione;
        return this;
    }

    public UserEntity setDescrizioneIfNotNull(String descrizione) {
        this.descrizione = descrizione != null ? descrizione : this.descrizione;
        return this;
    }

    public UserEntity setRuoloIfNotNull(String ruolo) {
        this.ruolo = ruolo != null ? ruolo : this.ruolo;
        return this;
    }

    public UserEntity setTelefonoIfNotNull(String telefono) {
        this.telefono = telefono != null ? telefono : this.telefono;
        return this;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", inizioContratto=" + inizioContratto +
                ", skillPrimario='" + skillPrimario + '\'' +
                ", cdc='" + cdc + '\'' +
                ", sedeEffettiva='" + sedeEffettiva + '\'' +
                ", livello='" + livello + '\'' +
                ", registratoIl=" + registratoIl +
                ", inizioRapporto=" + inizioRapporto +
                ", tipo='" + tipo + '\'' +
                ", skillSecondari='" + skillSecondari + '\'' +
                ", sedeAmministrativa='" + sedeAmministrativa + '\'' +
                ", capability='" + capability + '\'' +
                ", businessUnit='" + businessUnit + '\'' +
                ", ultimaConnessione=" + ultimaConnessione +
                ", ruolo='" + ruolo + '\'' +
                ", descrizione='" + descrizione + '\'' +
                ", telefono='" + telefono + '\'' +
                '}';
    }

    // Da rivedere quali attributi includere con precisione


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return uuid.equals(that.uuid) && userId.equals(that.userId) && name.equals(that.name) && email.equals(that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, userId, name, email);
    }
}
