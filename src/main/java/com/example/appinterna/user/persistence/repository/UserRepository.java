package com.example.appinterna.user.persistence.repository;

import com.example.appinterna.user.persistence.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
        Optional<UserEntity> findByUserId(Long userId);

        int deleteByUserId(Long userId);
}
