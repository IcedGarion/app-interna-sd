package com.example.appinterna.user.persistence.dao;

import com.example.appinterna.user.persistence.model.UserEntity;
import com.example.appinterna.user.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

// Creato per metodo custom: delete che ritorna entita' cancellata, cosa che Repository spring di default NON FA
@Component
public class UserDao {
    @Autowired
    private UserRepository repository;

    public List<UserEntity> findAllUsers() {
        return repository.findAll();
    }

    public UserEntity saveUser(UserEntity userEntity) {
        return repository.save(userEntity);
    }

    public Optional<UserEntity> findUserById(Long userId) {
        return repository.findByUserId(userId);
    }

    // custom
    public Optional<UserEntity> deleteUser(UserEntity userEntity) {
        return repository.deleteByUserId(userEntity.getUserId()) > 0
                ? Optional.of(userEntity)
                : Optional.empty();
    }
}
