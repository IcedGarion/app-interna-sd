package com.example.appinterna.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class appInternaApplication {

	public static void main(String[] args) {
		SpringApplication.run(appInternaApplication.class, args);
	}

}
