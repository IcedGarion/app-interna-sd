package com.example.appinterna.user.service.converter;

import com.example.appinterna.user.persistence.model.UserEntity;
import com.example.appinterna.user.service.bean.UserBean;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class UserEntityToBeanConverter {
    public UserBean convert(UserEntity userEntity) {
        return new UserBean(userEntity.getUuid(), userEntity.getUserId(), userEntity.getName(), userEntity.getEmail(), userEntity.getInizioContratto(), userEntity.getSkillPrimario(), userEntity.getCdc(), userEntity.getSedeEffettiva(), userEntity.getLivello(), userEntity.getRegistratoIl(), userEntity.getInizioRapporto(), userEntity.getTipo(), userEntity.getSkillSecondari(), userEntity.getSedeAmministrativa(), userEntity.getCapability(), userEntity.getBusinessUnit(), userEntity.getUltimaConnessione(), userEntity.getRuolo(), userEntity.getDescrizione(), userEntity.getTelefono());
    }
}
