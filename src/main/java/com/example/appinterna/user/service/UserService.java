package com.example.appinterna.user.service;

import com.example.appinterna.user.messaging.KafkaProducerUser;
import com.example.appinterna.user.messaging.bean.UserEventEnum;
import com.example.appinterna.user.service.bean.UserBean;
import com.example.appinterna.user.service.bean.UserUpdateBean;
import com.example.appinterna.user.persistence.dao.UserDao;
import com.example.appinterna.user.persistence.model.UserEntity;
import com.example.appinterna.user.service.bean.UserCreateBean;
import com.example.appinterna.user.service.converter.UserEntityToBeanConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserDao dao;

    @Autowired
    private UserEntityToBeanConverter userEntityToBeanConverter;

    @Autowired
    private KafkaProducerUser kafkaProducer;

    public List<UserBean> getAllUsers() {
        return dao.findAllUsers()
                .stream()
                .map(userEntityToBeanConverter::convert)
                .collect(Collectors.toList());
    }

    public Optional<UserBean> getUserById(Long userId) {
        return dao.findUserById(userId)
                .map(userEntityToBeanConverter::convert);
    }

    public Optional<UserBean> patchUpdateUser(UserUpdateBean userUpdateBean) {
        // servirebbe UserEntity BUILDER, invece che setName / setDescription modificate
        return dao.findUserById(userUpdateBean.getUserId())
                .map(existingUser -> existingUser.setNameIfNotNull(userUpdateBean.getName()))
                .map(existingUser -> existingUser.setEmailIfNotNull(userUpdateBean.getEmail()))
                .map(existingUser -> existingUser.setInizioContrattoIfNotNull(userUpdateBean.getInizioContratto()))
                .map(existingUser -> existingUser.setSkillPrimarioIfNotNull(userUpdateBean.getSkillPrimario()))
                .map(existingUser -> existingUser.setCdcIfNotNull(userUpdateBean.getCdc()))
                .map(existingUser -> existingUser.setSedeEffettivaIfNotNull(userUpdateBean.getSedeEffettiva()))
                .map(existingUser -> existingUser.setLivelloIfNotNull(userUpdateBean.getLivello()))
                .map(existingUser -> existingUser.setRegistratoIlIfNotNull(userUpdateBean.getRegistratoIl()))
                .map(existingUser -> existingUser.setInizioRapportoIfNotNull(userUpdateBean.getInizioRapporto()))
                .map(existingUser -> existingUser.setTipoIfNotNull(userUpdateBean.getTipo()))
                .map(existingUser -> existingUser.setSkillSecondariIfNotNull(userUpdateBean.getSkillSecondari()))
                .map(existingUser -> existingUser.setSedeAmministrativaIfNotNull(userUpdateBean.getSedeAmministrativa()))
                .map(existingUser -> existingUser.setCapabilityIfNotNull(userUpdateBean.getCapability()))
                .map(existingUser -> existingUser.setBusinessUnitIfNotNull(userUpdateBean.getBusinessUnit()))
                .map(existingUser -> existingUser.setUltimaConnessioneIfNotNull(userUpdateBean.getUltimaConnessione()))
                .map(existingUser -> existingUser.setRuoloIfNotNull(userUpdateBean.getRuolo()))
                .map(existingUser -> existingUser.setDescrizioneIfNotNull(userUpdateBean.getDescrizione()))
                .map(existingUser -> existingUser.setTelefonoIfNotNull(userUpdateBean.getTelefono()))
                .map(userEntityToBeanConverter::convert)
                .map(userBean -> kafkaProducer.send(UserEventEnum.USER_UPDATED, userBean));
    }

    // Da togliere
    @Transactional
    public Optional<UserBean> deleteUser(Long userId) {
        return dao.findUserById(userId)
                .flatMap(dao::deleteUser)
                .map(userEntityToBeanConverter::convert)
                .map(userBean -> kafkaProducer.send(UserEventEnum.USER_DELETED, userBean));
    }

    // Da togliere
    public Optional<UserBean> createUser(UserCreateBean userCreateBean) {
        return Optional.of(userCreateBean)
                .map(userCreate -> new UserEntity(userCreateBean.getName(), userCreateBean.getEmail(), userCreateBean.getInizioContratto(), userCreateBean.getSkillPrimario(), userCreateBean.getCdc(), userCreateBean.getSedeEffettiva(), userCreateBean.getLivello(), userCreateBean.getRegistratoIl(), userCreateBean.getInizioRapporto(), userCreateBean.getTipo(), userCreateBean.getSkillSecondari(), userCreateBean.getSedeAmministrativa(), userCreateBean.getCapability(), userCreateBean.getBusinessUnit(), userCreateBean.getUltimaConnessione(), userCreateBean.getRuolo(), userCreateBean.getDescrizione(), userCreateBean.getTelefono()))
                .map(dao::saveUser)
                .map(userEntityToBeanConverter::convert)
                .map(userBean -> kafkaProducer.send(UserEventEnum.USER_CREATED, userBean));
    }

    // Da togliere
    public Optional<UserBean> putUpdateUser(UserUpdateBean userUpdateBean) {
        return dao.findUserById(userUpdateBean.getUserId())
                .map(existingUser -> existingUser.setName(userUpdateBean.getName()))
                .map(existingUser -> existingUser.setEmail(userUpdateBean.getEmail()))
                .map(existingUser -> existingUser.setInizioContratto(userUpdateBean.getInizioContratto()))
                .map(existingUser -> existingUser.setSkillPrimario(userUpdateBean.getSkillPrimario()))
                .map(existingUser -> existingUser.setCdc(userUpdateBean.getCdc()))
                .map(existingUser -> existingUser.setSedeEffettiva(userUpdateBean.getSedeEffettiva()))
                .map(existingUser -> existingUser.setLivello(userUpdateBean.getLivello()))
                .map(existingUser -> existingUser.setRegistratoIl(userUpdateBean.getRegistratoIl()))
                .map(existingUser -> existingUser.setInizioRapporto(userUpdateBean.getInizioRapporto()))
                .map(existingUser -> existingUser.setTipo(userUpdateBean.getTipo()))
                .map(existingUser -> existingUser.setSkillSecondari(userUpdateBean.getSkillSecondari()))
                .map(existingUser -> existingUser.setSedeAmministrativa(userUpdateBean.getSedeAmministrativa()))
                .map(existingUser -> existingUser.setCapability(userUpdateBean.getCapability()))
                .map(existingUser -> existingUser.setBusinessUnit(userUpdateBean.getBusinessUnit()))
                .map(existingUser -> existingUser.setUltimaConnessione(userUpdateBean.getUltimaConnessione()))
                .map(existingUser -> existingUser.setRuolo(userUpdateBean.getRuolo()))
                .map(existingUser -> existingUser.setDescrizione(userUpdateBean.getDescrizione()))
                .map(existingUser -> existingUser.setTelefono(userUpdateBean.getTelefono()))
                .map(dao::saveUser)
                .map(userEntityToBeanConverter::convert)
                .map(userBean -> kafkaProducer.send(UserEventEnum.USER_UPDATED, userBean))
                // se dao.findUserById ritorna Optional.Empty(), allora crea nuovo utente e ritorna Optional.Empty()
                .or(() -> Optional.of(userUpdateBean)
                        .map(newUser -> new UserEntity(userUpdateBean.getUserId(), userUpdateBean.getName(), userUpdateBean.getEmail(), userUpdateBean.getInizioContratto(), userUpdateBean.getSkillPrimario(), userUpdateBean.getCdc(), userUpdateBean.getSedeEffettiva(), userUpdateBean.getLivello(), userUpdateBean.getRegistratoIl(), userUpdateBean.getInizioRapporto(), userUpdateBean.getTipo(), userUpdateBean.getSkillSecondari(), userUpdateBean.getSedeAmministrativa(), userUpdateBean.getCapability(), userUpdateBean.getBusinessUnit(), userUpdateBean.getUltimaConnessione(), userUpdateBean.getRuolo(), userUpdateBean.getDescrizione(), userUpdateBean.getTelefono()))
                        .map(dao::saveUser)
                        .map(userEntityToBeanConverter::convert)
                        .map(userBean -> kafkaProducer.send(UserEventEnum.USER_CREATED, userBean)).
                        .flatMap(newUser -> Optional.empty()));
    }
}
