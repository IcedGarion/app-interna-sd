package com.example.appinterna.user.messaging.bean;

public enum UserEventEnum {
    USER_CREATED,
    USER_UPDATED,
    USER_DELETED
}
