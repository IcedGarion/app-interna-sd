package com.example.appinterna.user.messaging.converter;

import com.example.appinterna.user.messaging.bean.UserMessageBean;
import com.example.appinterna.user.service.bean.UserBean;
import org.springframework.stereotype.Component;

@Component
public class MessageToUserBeanConverter {
    public UserBean convert(UserMessageBean userMessageBean) {
        // converte mantenendo tutti gli attributi
        return new UserBean(userMessageBean.getUuid(), userMessageBean.getUserId(), userMessageBean.getName(), userMessageBean.getEmail(), userMessageBean.getInizioContratto(), userMessageBean.getSkillPrimario(), userMessageBean.getCdc(), userMessageBean.getSedeEffettiva(), userMessageBean.getLivello(), userMessageBean.getRegistratoIl(), userMessageBean.getInizioRapporto(), userMessageBean.getTipo(), userMessageBean.getSkillSecondari(), userMessageBean.getSedeAmministrativa(), userMessageBean.getCapability(), userMessageBean.getBusinessUnit(), userMessageBean.getUltimaConnessione(), userMessageBean.getRuolo(), userMessageBean.getDescrizione(), userMessageBean.getTelefono());
    }
}
