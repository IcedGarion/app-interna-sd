package com.example.appinterna.user.messaging.converter;

import com.example.appinterna.user.messaging.bean.UserMessageBean;
import com.example.appinterna.user.service.bean.UserBean;
import org.springframework.stereotype.Component;

@Component
public class UserBeanToMessageConverter {
    public UserMessageBean convert(UserBean userBean) {
        // converte mantenendo tutti gli attributi
        return new UserMessageBean(userBean.getUuid(), userBean.getUserId(), userBean.getName(), userBean.getEmail(), userBean.getInizioContratto(), userBean.getSkillPrimario(), userBean.getCdc(), userBean.getSedeEffettiva(), userBean.getLivello(), userBean.getRegistratoIl(), userBean.getInizioRapporto(), userBean.getTipo(), userBean.getSkillSecondari(), userBean.getSedeAmministrativa(), userBean.getCapability(), userBean.getBusinessUnit(), userBean.getUltimaConnessione(), userBean.getRuolo(), userBean.getDescrizione(), userBean.getTelefono());
    }
}
