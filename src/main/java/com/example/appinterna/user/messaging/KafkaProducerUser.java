package com.example.appinterna.user.messaging;

import com.example.appinterna.user.messaging.bean.UserEventEnum;
import com.example.appinterna.user.messaging.converter.UserBeanToMessageConverter;
import com.example.appinterna.user.service.bean.UserBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaProducerUser{
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerUser.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    private UserBeanToMessageConverter converter;

    public UserBean send(UserEventEnum event, UserBean payload) {
        // converte e serializza
        try {
            kafkaTemplate.send(KafkaConfig.USER_KAFKA_TOPIC, event.name(), new ObjectMapper().writeValueAsString(converter.convert(payload)));
        }
        catch (JsonProcessingException e) {
            LOGGER.error("Unable to serialize object: '{}'", payload);
        }

        return payload;
    }
}
