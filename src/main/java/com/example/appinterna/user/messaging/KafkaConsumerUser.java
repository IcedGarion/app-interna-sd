package com.example.appinterna.user.messaging;

import com.example.appinterna.user.messaging.bean.UserEventEnum;
import com.example.appinterna.user.messaging.bean.UserMessageBean;
import com.example.appinterna.user.messaging.converter.MessageToUserBeanConverter;
import com.example.appinterna.user.service.bean.UserBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class KafkaConsumerUser {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumerUser.class);

    private CountDownLatch latch = new CountDownLatch(1);

    // contiene info di contorno al messaggio (topic, event, string payload)
    private ConsumerRecord<String, String> message = null;

    // contiene oggetto deserializzato
    private UserBean payload = null;

    private UserEventEnum event = null;

    @Autowired
    private MessageToUserBeanConverter converter;

    @KafkaListener(topics = KafkaConfig.USER_KAFKA_TOPIC, groupId = "groupId")
    public void receive(ConsumerRecord<String, String> consumerRecord) {
        setMessage(consumerRecord);
        setEvent(UserEventEnum.valueOf(message.key()));

        // deserializza e converte
        try {
            setPayload(converter.convert(new ObjectMapper().readValue(consumerRecord.value(), UserMessageBean.class)));
        }
        catch (JsonProcessingException e) {
            LOGGER.error("Unable to parse message: '{}'", consumerRecord.value());
        }

        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public ConsumerRecord<String, String> getMessage() {
        return message;
    }

    private void setMessage(ConsumerRecord<String, String> message) {
        this.message = message;
    }

    public UserBean getPayload() { return payload; }

    public void setPayload(UserBean payload) { this.payload = payload; }

    public UserEventEnum getEvent() {
        return event;
    }

    public void setEvent(UserEventEnum event) {
        this.event = event;
    }
}
