package com.example.appinterna.user.controller.bean;

import java.util.Date;

public class UserCreateOrUpdateRequestBean {
    private String name;
    private String email;
    private Date inizioContratto;
    private String skillPrimario;
    private String cdc;
    private String sedeEffettiva;
    private String livello;
    private Date registratoIl;
    private Date inizioRapporto;
    private String tipo;
    private String skillSecondari;
    private String sedeAmministrativa;
    private String capability;
    private String businessUnit;
    private Date ultimaConnessione;
    private String ruolo;
    private String descrizione;
    private String telefono;

    // Usato dai test per deserializzare
    public UserCreateOrUpdateRequestBean() {}

    public UserCreateOrUpdateRequestBean(String name, String email, Date inizioContratto, String skillPrimario, String cdc, String sedeEffettiva, String livello, Date registratoIl, Date inizioRapporto, String tipo, String skillSecondari, String sedeAmministrativa, String capability, String businessUnit, Date ultimaConnessione, String ruolo, String descrizione, String telefono) {
        this.name = name;
        this.email = email;
        this.inizioContratto = inizioContratto;
        this.skillPrimario = skillPrimario;
        this.cdc = cdc;
        this.sedeEffettiva = sedeEffettiva;
        this.livello = livello;
        this.registratoIl = registratoIl;
        this.inizioRapporto = inizioRapporto;
        this.tipo = tipo;
        this.skillSecondari = skillSecondari;
        this.sedeAmministrativa = sedeAmministrativa;
        this.capability = capability;
        this.businessUnit = businessUnit;
        this.ultimaConnessione = ultimaConnessione;
        this.ruolo = ruolo;
        this.descrizione = descrizione;
        this.telefono = telefono;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Date getInizioContratto() {
        return inizioContratto;
    }

    public String getSkillPrimario() {
        return skillPrimario;
    }

    public String getCdc() {
        return cdc;
    }

    public String getSedeEffettiva() {
        return sedeEffettiva;
    }

    public String getLivello() {
        return livello;
    }

    public Date getRegistratoIl() {
        return registratoIl;
    }

    public Date getInizioRapporto() {
        return inizioRapporto;
    }

    public String getTipo() {
        return tipo;
    }

    public String getSkillSecondari() {
        return skillSecondari;
    }

    public String getSedeAmministrativa() {
        return sedeAmministrativa;
    }

    public String getCapability() {
        return capability;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public Date getUltimaConnessione() {
        return ultimaConnessione;
    }

    public String getRuolo() {
        return ruolo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public String getTelefono() {
        return telefono;
    }
}
