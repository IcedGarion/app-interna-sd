package com.example.appinterna.user.controller.converter;

import com.example.appinterna.user.controller.bean.UserResponseBean;
import com.example.appinterna.user.service.bean.UserBean;
import org.springframework.stereotype.Component;

@Component
public class UserBeanToResponseConverter {
    public UserResponseBean convert(UserBean userBean) {
        return new UserResponseBean(userBean.getUuid(),userBean.getName(), userBean.getEmail(), userBean.getInizioContratto(), userBean.getSkillPrimario(), userBean.getCdc(), userBean.getSedeEffettiva(), userBean.getLivello(), userBean.getRegistratoIl(), userBean.getInizioRapporto(), userBean.getTipo(), userBean.getSkillSecondari(), userBean.getSedeAmministrativa(), userBean.getCapability(), userBean.getBusinessUnit(), userBean.getUltimaConnessione(), userBean.getRuolo(), userBean.getDescrizione(), userBean.getTelefono());
    }
}
