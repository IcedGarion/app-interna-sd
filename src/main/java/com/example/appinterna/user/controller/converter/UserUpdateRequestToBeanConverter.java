package com.example.appinterna.user.controller.converter;

import com.example.appinterna.user.controller.bean.UserCreateOrUpdateRequestBean;
import com.example.appinterna.user.service.bean.UserUpdateBean;
import org.springframework.stereotype.Component;

@Component
public class UserUpdateRequestToBeanConverter {
    public UserUpdateBean convert(Long userId, UserCreateOrUpdateRequestBean userCreateOrUpdateRequestBean) {
        return new UserUpdateBean(userId, userCreateOrUpdateRequestBean.getName(), userCreateOrUpdateRequestBean.getEmail(), userCreateOrUpdateRequestBean.getInizioContratto(), userCreateOrUpdateRequestBean.getSkillPrimario(), userCreateOrUpdateRequestBean.getCdc(), userCreateOrUpdateRequestBean.getSedeEffettiva(), userCreateOrUpdateRequestBean.getLivello(), userCreateOrUpdateRequestBean.getRegistratoIl(), userCreateOrUpdateRequestBean.getInizioRapporto(), userCreateOrUpdateRequestBean.getTipo(), userCreateOrUpdateRequestBean.getSkillSecondari(), userCreateOrUpdateRequestBean.getSedeAmministrativa(), userCreateOrUpdateRequestBean.getCapability(), userCreateOrUpdateRequestBean.getBusinessUnit(), userCreateOrUpdateRequestBean.getUltimaConnessione(), userCreateOrUpdateRequestBean.getRuolo(), userCreateOrUpdateRequestBean.getDescrizione(), userCreateOrUpdateRequestBean.getTelefono());
    }
}
