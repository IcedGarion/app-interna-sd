package com.example.appinterna.user.controller;

import com.example.appinterna.user.controller.bean.UserResponseBean;
import com.example.appinterna.user.controller.bean.UserCreateOrUpdateRequestBean;
import com.example.appinterna.user.controller.converter.UserBeanToResponseConverter;
import com.example.appinterna.user.controller.converter.UserCreateRequestToBeanConverter;
import com.example.appinterna.user.controller.converter.UserRequestToResponseConverter;
import com.example.appinterna.user.service.UserService;
import com.example.appinterna.user.controller.converter.UserUpdateRequestToBeanConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UserDelegate {
    @Autowired
    private UserService userService;

    @Autowired
    private UserCreateRequestToBeanConverter userCreateRequestToBeanConverter;
    @Autowired
    private UserUpdateRequestToBeanConverter userUpdateRequestToBeanConverter;
    @Autowired
    private UserRequestToResponseConverter userRequestToResponseConverter;
    @Autowired
    private UserBeanToResponseConverter userBeanToResponseConverter;

    public ResponseEntity<List<UserResponseBean>> getAllUsers() {
        return Optional.of(
                userService.getAllUsers()
                .stream()
                .map(userBeanToResponseConverter::convert)
                .collect(Collectors.toList()))
                .map(ResponseEntity::ok)
                .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    public ResponseEntity<UserResponseBean> getUserById(Long userId) {
        return userService.getUserById(userId)
                .map(userBeanToResponseConverter::convert)
                .map(ResponseEntity::ok)
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    public ResponseEntity<UserResponseBean> patchUpdateUser(Long userId, UserCreateOrUpdateRequestBean userCreateOrUpdateRequestBean) {
        return Optional.of(userCreateOrUpdateRequestBean)
                .map(request -> userUpdateRequestToBeanConverter.convert(userId, request))
                .flatMap(userService::patchUpdateUser)
                .map(userBeanToResponseConverter::convert)
                .map(ResponseEntity::ok)
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    // Da togliere
    public ResponseEntity<UserResponseBean> deleteUser(Long userId) {
        return userService.deleteUser(userId)
                .map(userBeanToResponseConverter::convert)
                .map(ResponseEntity::ok)
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    // Da togliere
    public ResponseEntity<UserResponseBean> createUser(UserCreateOrUpdateRequestBean userCreateRequestBean) {
        return Optional.of(userCreateRequestBean)
                .map(userCreateRequestToBeanConverter::convert)
                .flatMap(userService::createUser)
                .map(userBeanToResponseConverter::convert)
                .map(response -> new ResponseEntity<>(response, HttpStatus.CREATED))
                .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    // Da togliere
    public ResponseEntity<UserResponseBean> putUpdateUser(Long userId, UserCreateOrUpdateRequestBean userCreateOrUpdateRequestBean) {
        return Optional.of(userCreateOrUpdateRequestBean)
                .map(request -> userUpdateRequestToBeanConverter.convert(userId, request))
                .flatMap(userService::putUpdateUser)
                .map(userBeanToResponseConverter::convert)
                .map(ResponseEntity::ok)
                // se userService::putUpdateUser ritorna Optional.Empty(), allora qua ritorna HttpStatus.CREATED
                .orElseGet(() -> Optional.of(userCreateOrUpdateRequestBean)
                        .map(userRequestToResponseConverter::convert)
                        .map(response -> new ResponseEntity<>(response, HttpStatus.CREATED))
                        .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST)));
    }
}
