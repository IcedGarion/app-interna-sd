package com.example.appinterna.user.controller.converter;

import com.example.appinterna.user.controller.bean.UserCreateOrUpdateRequestBean;
import com.example.appinterna.user.service.bean.UserCreateBean;
import org.springframework.stereotype.Component;

@Component
public class UserCreateRequestToBeanConverter {
    public UserCreateBean convert(UserCreateOrUpdateRequestBean userCreateRequestBean) {
        return new UserCreateBean(userCreateRequestBean.getName(), userCreateRequestBean.getEmail(), userCreateRequestBean.getInizioContratto(), userCreateRequestBean.getSkillPrimario(), userCreateRequestBean.getCdc(), userCreateRequestBean.getSedeEffettiva(), userCreateRequestBean.getLivello(), userCreateRequestBean.getRegistratoIl(), userCreateRequestBean.getInizioRapporto(), userCreateRequestBean.getTipo(), userCreateRequestBean.getSkillSecondari(), userCreateRequestBean.getSedeAmministrativa(), userCreateRequestBean.getCapability(), userCreateRequestBean.getBusinessUnit(), userCreateRequestBean.getUltimaConnessione(), userCreateRequestBean.getRuolo(), userCreateRequestBean.getDescrizione(), userCreateRequestBean.getTelefono());
    }
}
