package com.example.appinterna.user.controller.converter;

import com.example.appinterna.user.controller.bean.UserCreateOrUpdateRequestBean;
import com.example.appinterna.user.controller.bean.UserResponseBean;
import org.springframework.stereotype.Component;

import java.util.Date;

// converte request to response, usato dalla PUT: se user non esisteva e quindi viene creato, risponde direttamente con i campi settati dalla request
@Component
public class UserRequestToResponseConverter {

    public UserResponseBean convert(UserCreateOrUpdateRequestBean userCreateOrUpdateRequestBean) {
        return new UserResponseBean(userCreateOrUpdateRequestBean.getName(), userCreateOrUpdateRequestBean.getEmail(), userCreateOrUpdateRequestBean.getInizioContratto(), userCreateOrUpdateRequestBean.getSkillPrimario(), userCreateOrUpdateRequestBean.getCdc(), userCreateOrUpdateRequestBean.getSedeEffettiva(), userCreateOrUpdateRequestBean.getLivello(), userCreateOrUpdateRequestBean.getRegistratoIl(), userCreateOrUpdateRequestBean.getInizioRapporto(), userCreateOrUpdateRequestBean.getTipo(), userCreateOrUpdateRequestBean.getSkillSecondari(), userCreateOrUpdateRequestBean.getSedeAmministrativa(), userCreateOrUpdateRequestBean.getCapability(), userCreateOrUpdateRequestBean.getBusinessUnit(), userCreateOrUpdateRequestBean.getUltimaConnessione(), userCreateOrUpdateRequestBean.getRuolo(), userCreateOrUpdateRequestBean.getDescrizione(), userCreateOrUpdateRequestBean.getTelefono());
    }
}
