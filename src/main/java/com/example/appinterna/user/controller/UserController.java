package com.example.appinterna.user.controller;

import com.example.appinterna.user.controller.bean.UserResponseBean;
import com.example.appinterna.user.controller.bean.UserCreateOrUpdateRequestBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserDelegate userDelegate;

    @GetMapping("/api/users")
    public ResponseEntity<List<UserResponseBean>> getAllUsers() {
        return userDelegate.getAllUsers();
    }

    @GetMapping("/api/users/{userId}")
    public ResponseEntity<UserResponseBean> getUserById(@PathVariable Long userId) {
        return userDelegate.getUserById(userId);
    }

    @PatchMapping("/api/users/{userId}")
    public ResponseEntity<UserResponseBean> patchUpdateUser(@PathVariable Long userId, @RequestBody UserCreateOrUpdateRequestBean userRequestBean) {
        return userDelegate.patchUpdateUser(userId, userRequestBean);
    }

    // Da togliere
    @DeleteMapping("/api/users/{userId}")
    public ResponseEntity<UserResponseBean> deleteUser(@PathVariable Long userId) {
        return userDelegate.deleteUser(userId);
    }

    // Da togliere
    @PutMapping("/api/users/{userId}")
    public ResponseEntity<UserResponseBean> putUpdateUser(@PathVariable Long userId, @RequestBody UserCreateOrUpdateRequestBean userRequestBean) {
        return userDelegate.putUpdateUser(userId, userRequestBean);
    }

    // Da togliere
    @PostMapping("/api/users")
    public ResponseEntity<UserResponseBean> createUser(@RequestBody UserCreateOrUpdateRequestBean userRequestBean) {
        return userDelegate.createUser(userRequestBean);
    }
}